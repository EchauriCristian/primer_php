            <div class="row col-12 col-sm-12 offset-md-2 col-md-8 offset-lg-2 col-lg-8 offset-xl-2 col-xl-8">
				<form id="contact-form" name="contact-form">

					<div class="form-group">
						<label for="name">Nombre:</label><br/>
						<input type="text" id="name" name="name">
					</div>

					<div class="form-group">
						<label for="email">Correo electronico:</label><br/>
						<input type="text" id="email" name="email">
					</div>

					<div class="form-group">
						<label for="phone">Telefono:</label><br/>
						<input type="text" id="phone" name="phone">
					</div>

					<!-- name="value[]" nor permite coger en FormData varios valores para la misma key, como por ejemplo en un select-->
					<div class="form-group">
						<label for="technology[]">Tecnologia(s) a consultar:</label><br/>
						<select class="contact-select select2" name="technology[]" multiple="multiple">
							<optgroup label="Web development">
								<option value="html">HTML</option>
								<option value="css">CSS</option>
								<option value="javascript">JavaScript</option>
							</optgroup>
							<optgroup label="Distributed development">
								<option value="java">Java</option>
								<option value="C#">C#</option>
								<option value=""></option>
							</optgroup>
						</select>
					</div>

					<div class="form-group">
						<label for="comentary">Mensaje:</label><br/>
						<textarea id="comentary" name="comentary"></textarea>
					</div>

					<input type="submit" id="submit" value="Enviar">
				</form>
				
			</div>