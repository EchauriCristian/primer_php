
			<!-- Modal de contacto -->
			<!-- Modal de Bootstrap para contactar via email con la web y no ser direccionado en el proceso a una página "contacto" -->
			<!-- Debido a como funciona el stackeo de z-index, el modal debe ir encima de la barra de navegación -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
					
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Contact Us</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
					
						<div class="modal-body">
							<form>
								<!-- El uso de la clase form-group de bootstap ayuda a un estilo visual de bloque más claro -->
								<div class="form-group">
									<label for="userEmail">
										Email address:
									</label>
									<input type="email" class="form-control" id="userEmail" aria-describedby="emailHelp" placeholder="Enter email">
									<small id="emailHelp" class="form-text text-muter">We'll never share your email with anyone else.</small>
								</div>
								
								<div class="form-group">
									<label for="emailTextarea">
										Message:
									</label>
									<textarea class="form-control" id="emailTextarea" rows="3"></textarea>	
								</div>
							</form>
						</div>
					
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Send</button>
						</div>
					</div>
				</div>
			</div>


			<!-- Barra de navegacion -->
			<nav class="navbar navbar-expand-lg navbar-light">

				<a class="navbar-brand">
					<img class="logo-img" src="img/logo.png">
				</a>

				<!-- Boton para abrir el menu de navegacion en resoluciones pequeñas-->
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">

					<ul class="navbar-nav mr-auto">

						<li class="nav-item">
							<a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Our projects
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Project 1</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Project 2</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Project 3</a>
							</div>
						</li>

						<!--
							Link trigger modal 
							Cambiado el button por defecto de bootstrap por un link para mantener el diseño del nav 
							<button type="button" class="nav-button" data-toggle="modal" data-target="#exampleModal"></button>
							-->
						<li class="nav-item">
							<a href="#exampleModal" class="nav-link" data-toggle="modal">Contact</a>		
						</li>

										
					</ul>
				</div>
            </nav>