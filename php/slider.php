<!-- Slider -->
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 slider-container">
			
				<div id="carouselExampleControls" class="offset-md-2 col-md-8 offset-lg-3 col-lg-6 offset-xl-3 col-xl-6 carousel slide" data-ride="carousel">

					<div class="carousel-inner">
					
						<div class="carousel-item active">

							<div class="row single-slide">
							
								<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
									<img class="slider-img" src="img/slider/slider1.png">
								</div>
								
								<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
									<h3 class="slider-text-tittle">Full-Stack development</h3>
									<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
								</div>
								
							</div>
						</div>
						
						
						<div class="carousel-item">
						
							<div class="row single-slide">
							
								<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
									<img class="slider-img" src="img/slider/slider2.png">
								</div>

								<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
									<h3 class="slider-text-tittle">Responsive design</h3>
									<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
								</div>
								
							</div>
						</div>
						

						<div class="carousel-item">
						
							<div class="row single-slide">
							
								<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
									<img class="slider-img" src="img/slider/slider3.png">
								</div>
								
								<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
									<h3 class="slider-text-tittle">E-commerce</h3>
									<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
								</div>
							</div>
						</div>
						
					</div>
					
					
					<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					
					
					<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
					
				</div>
			</div>